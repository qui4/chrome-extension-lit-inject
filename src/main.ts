/*
 * Chrome will prevent native registerElement from being executed within a chrome extension. By default
 * window.customElements will be null when accessed within a chrome extension. The @webcomponents/custom-elements
 * polyfill will make sure window.customElements can be used.
 */
import '@webcomponents/custom-elements';
import { html, render } from 'lit';
import { RootElement } from './elements/root-element';

// Define a root custom-element
customElements.define('root-element', RootElement);

// Render the root-custom element in the document body using the lit-html render engine
render(html`<root-element></root-element>`, document.body);

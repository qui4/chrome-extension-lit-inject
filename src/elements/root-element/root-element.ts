import { LitElement, html } from 'lit';
import { ScopedElementsMixin } from '@open-wc/scoped-elements';
import { ChildElement } from '../child-element';
import { styles } from './root-element.styles';

export class RootElement extends ScopedElementsMixin(LitElement) {
  static get scopedElements() {
    return {
      'child-element': ChildElement,
    };
  }

  static override styles = styles;

  override render() {
    return html`
      <section>
        <p>root-element</p>
        <child-element
          @some-event=${() => alert('in event handler!')}
        ></child-element>
      </section>
    `;
  }
}

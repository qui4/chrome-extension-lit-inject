import { css } from 'lit';

export const styles = css`
  :host {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 2147483647;
    pointer-events: none;
    overflow-y: hidden;
  }

  :host * {
    pointer-events: auto;
  }
`;

import { LitElement, html } from 'lit';

export class ChildElement extends LitElement {
  override render() {
    return html`
      <section>
        child-element
        <button @click=${this._sendSomeEvent.bind(this)}>send event</button>
      </section>
    `;
  }

  _sendSomeEvent() {
    this.dispatchEvent(new CustomEvent('some-event'));
  }
}
